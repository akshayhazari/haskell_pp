import Control.Monad
import Control.Monad.State
import Control.Applicative
import System.Random
import Data.List

data ArithExp = Value Int | Add ArithExp ArithExp |Sub ArithExp ArithExp | Mul ArithExp ArithExp 

assign x = Value x

evalX :: ArithExp -> State String Int
evalX (Value v)        =  do
  modify (++(" Value "++(show v)))
  return (v) 
evalX (Add ax1 ax2)   =  do
  modify (++" ( ")  
  modify (++"Add ")
  val1 <- evalX ax1
  val2 <- evalX ax2
  modify (++" ) ")
  return (val1 + val2)
evalX (Sub ax1 ax2)   =  do
  modify (++" ( ")  
  modify (++"Sub ")
  val1 <- evalX ax1
  val2 <- evalX ax2
  modify (++" ) ")
  return (val1 - val2)  
evalX (Mul ax1 ax2)   =  do
  modify (++ " ( ")  
  modify (++"Mul ")
  val1 <- evalX ax1
  val2 <- evalX ax2
  modify (++" ) ")
  return (val1 * val2)

randomEval val exp1 exp2 =
  case val of 
    2 -> evalX (Add exp1 exp2)
    3 -> evalX (Sub exp1 exp2)
    4 -> evalX (Mul exp1 exp2)

  
fx :: Int -> Int -> IO [Int]  
fx md s = do  
  x <- randomRIO (1,min md s)
  if md==1 
     then return [md]
     else liftM (md:) (fx (md-x) s)     
    
mySplitAt:: Maybe Int -> String -> (String,String) 
mySplitAt (Just x) str = splitAt (x+1) str

expressionEval :: IO ()
expressionEval = do
  randomExpressions <- readFile "Random_Data/operators.txt"
  rValues <- readFile "Random_Data/values.txt"
  contents <- readFile "Config/configFile.txt"
  let config =  (map (read.snd) $ zipWith (mySplitAt) (map (elemIndex '=') $ lines contents) (lines contents) :: [Int])
  let noe = config !! 0 ; mind = config !! 1 ; maxd = config !! 2 ; sf = config !! 3
  let result = zipWith (\x y -> randomCreate x y maxd sf) (read(randomExpressions)::[[Int]]) (read(rValues)::[[Int]])
  exprsns <- sequence $ result
  writeFile "Result/result.txt" $ show $ map (\x -> runState (evalX x) []) exprsns
  where randomCreate (a:as) (b:c:cs) md sf = do  
          r1 <- randomRIO (1,min md sf)
          r2 <- randomRIO (1,min md sf)
          case md<=1 of 
            True -> case a of 
              2 -> return (Add (Value b) (Value c))
              3 -> return (Sub (Value b) (Value c))
              4 -> return (Mul (Value b) (Value c))
            _ -> case a of  
              2 -> do  
                l <- (randomCreate as (b:c:cs) (md-r1) sf)
                r <- (randomCreate as (cs) (md-r2) sf)
                return (Add l r)
              3 -> do
                l <- (randomCreate as (b:c:cs) (md-r1) sf)
                r <- (randomCreate as (cs) (md-r2) sf)
                return (Sub l r)
              4 -> do 
                l <- (randomCreate as (b:c:cs) (md-r1) sf)
                r <- (randomCreate as (cs) (md-r2) sf)
                return (Mul l r)

main :: IO ()
main = do
  expressionEval
