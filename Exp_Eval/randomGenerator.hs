import Control.Monad
import Control.Monad.State
import Control.Applicative
import System.Random
import Data.List

randNo :: Int -> Int -> IO Int
randNo mind maxd = randomRIO (mind,maxd)
randomFile :: Int -> Int -> Int -> FilePath-> IO ()
randomFile mind maxd noe filename= do
  l<-replicateM noe ( randomRIO (maxd,maxd))
  l1 <- mapM (`replicateM` (randomRIO ((0,9999)::(Int,Int)))) (map (\x -> 2 ^ x) l)
  writeFile filename (show l1)

randomExp :: Int -> Int -> FilePath -> IO ()  
randomExp maxd noe fn = do
  l<-replicateM (noe) $ replicateM ((2^maxd)::Int) ( randomRIO ((2,4)::(Int,Int)))
  writeFile fn (show l)

mySplitAt:: Maybe Int -> String -> (String,String) 
mySplitAt (Just x) str = splitAt (x+1) str

generate :: IO ()
generate = do
  contents <- readFile "Config/configFile.txt"
  let config =  (map (read.snd) $ zipWith (mySplitAt) (map (elemIndex '=') $ lines contents) (lines contents) :: [Int])
  let noe = config !! 0 ; mind = config !! 1 ; maxd = config !! 2 ; sf = config !! 3
  randomFile mind maxd noe "Random_Data/values.txt"                                  
  randomExp maxd noe "Random_Data/operators.txt"                                     
  
main :: IO ()  
main = do
  generate