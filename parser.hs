import Data.Char

data State =  InBraces Int | Expression   

--parser Expression "((3+2)*(5-3))+3*7" [] []

numberSplit "" = ("","") 
numberSplit string = (takeWhile (isDigit) string,dropWhile (isDigit) string)

number "" = 0
number string  = foldl (\acc x-> acc*10 + digitToInt(x)) 0 string

evalStack v [] = (v,[])
evalStack (valstack) (o:opStack) 
  | o == '(' = (valstack,opStack)
  | o == '+' = evalStack ((sum $ take 2 valstack):(drop 2 valstack)) opStack
  | o == '*' = evalStack (a:(drop 2 valstack)) opStack              
  | o == '-' = evalStack (b:(drop 2 valstack)) opStack
  where  a = (valstack !! 0) * (valstack !! 1)     
         b = (valstack !! 1) - (valstack !! 0)     

evalExp v [] = v !! 0 
evalExp valStack (o:opStack)  
  | o == '+' = evalExp ((sum $ take 2 valStack):(drop 2 valStack)) opStack
  | o == '*' = evalExp (a:(drop 2 valStack)) opStack              
  | o == '-' = evalExp (b:(drop 2 valStack)) opStack
  where  a = (valStack !! 0) * (valStack !! 1)     
         b = (valStack !! 1) - (valStack !! 0)     

parser Expression "" v o =  evalExp v o 
parser Expression (s:str) valStack opStack  
  |  isDigit (s) = parser Expression (b) (number(a):valStack) opStack 
  |  s =='(' = parser (InBraces 1)  str valStack ('(':opStack) 
  |  otherwise = parser Expression str valStack (s:opStack) 
  where (a,b) = numberSplit (s:str) 

parser (InBraces x) (s:str) valStack opStack
  |  isDigit (s) = parser (InBraces x) (d) (number(c):valStack) opStack 
  |  s == '(' = parser (InBraces (x+1)) str valStack opStack
  |  s == ')' = if (x==1) then parser Expression str a b else parser (InBraces (x-1)) str a b  
  |  otherwise = parser (InBraces x) str valStack (s:opStack) 
  where (a,b) = evalStack valStack opStack
        (c,d) = numberSplit (s:str) 


--parser Number (s:str) = (if isDigit (s) digitToInt (s) + parser (str) Number)    